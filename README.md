# README #

This application was built with DropWizard Framework that uses Jetty for HTTP Server, Jersey for REST and Jackson for JSON.
It's an application that provides some REST api's with CRUD operations over entities PRODUCT and IMAGE.

### What is this repository for? ###
This repository has an application that will be evaluated by Avenue Code Company in a selection process to Developer Java position.

The technical specification are:
1) Maven must be used to build, run tests and start the application.
2) The tests must be started with the mvn test command.
3) The application must start with a Maven command: mvn exec:java, mvn jetty:run, mvn spring-boot:run, etc.
4) The application must have a stateless API and use a database to store data.
5) An embedded in-memory database should be used: either H2, HSQL, SQLite or Derby.
6) The database and tables creation should be done by Maven or by the application.
7) You must provide BitBucket username. A free BitBucket account can be created at http://bitbucket.org. Once finished, you must give the user ac-recruitment read permission on your repository so that you can be evaluated.
8) You must provide a README.txt (plain text) or a README.md (Markdown) file at the root of your repository, explaining:
   - How to compile and run the application with an example for each call.
   - How to run the suite of automated tests.
   - Mention anything that was asked but not delivered and why, and any additional comments.

Obs(1): I used EclipseLink as JPA implementation because I have more experience with this framework and wouldn't want to got some issue that I wouldn't know to solve with time offered.

Obs(2): I didn't know how and not discovered how to run Tests Suite with Maven.

### How do I get set up? ###

To execute:
  mvn exec:java

Execute command package:
**without tests:**
  mvn package -Dmaven.test.skip=true
**with tests:**
  mvn package -DskipTests

Execute command test:
  mvn test

Available API's:

	1. Inserting new product:
	HTTP METHOD: POST:
	Content-Type: application/json
	http://localhost:8080/avenue-code/apis/product/
	{"name" : "Product 01"}

	2. Inserting new product with parent product:
	HTTP METHOD: POST
	Content-Type: application/json
	{"name" : "Product 03", "parent" : { "id" : 1 } }

	3. Inserting new product with parent product and images:
	HTTP METHOD: POST
	Content-Type: application/json
	{"name" : "Product 03", "parent" : { "id" : 1 }, "images" : [ {"name" : "image_1.png"}, {"name" : "image_2.png"}, {"name" : "image_3.png"} ]}

	4. Getting a product by id:
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/product/1

	5. Deleting a product by id:
	HTTP METHOD: DELETE
	http://localhost:8080/avenue-code/apis/product/1

	6. Updating a product:
	HTTP METHOD: PUT
	http://localhost:8080/avenue-code/apis/product
	{ "id" : 1,"name" : "Product renamed."}

	7. Getting a product by id with parent product:
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/product/6/parent

	8. Getting a product by id with images:
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/product/9/images

	9. Getting a product by id with parent product and images:
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/product/9/parent/images

	10. Getting all products(without parent product and children):
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/products

	11. Inserting new product with children product's:
	HTTP METHOD: POST
	{"name" : "Product 15", "leaves" : [ {"name" : "Product 20"}, {"name" : "Product 30"}, {"name" : "Product 40"} ] }

	12. Getting all products(with parent product and without children product):
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/products/parent

	13. Getting all products(without parent node and with images):
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/products/images

	14. Getting all products(with parent node, children nodes and images):
	HTTP METHOD: GET
	http://localhost:8080/avenue-code/apis/products/parent/images

### Who do I talk to? ###

* Repo owner: magno82br@yahoo.com.br