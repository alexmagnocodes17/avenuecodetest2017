package com.avenue.code.dropwizard;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.avenue.code.resources.AvenueCodeResource;
import com.avenue.code.resources.HelloWorldResource;

/*
 * To execute : java -jar sample-dropwizard-0.0.1-SNAPSHOT.jar server
 * example.yml
 */
public class AvenueCodeApplication extends Application<AvenueCodeConfiguration>
{
    public static void main(String[] args) throws Exception
    {
        new AvenueCodeApplication().run(args);
    }

    @Override
    public String getName()
    {
        return "Avenue Code Test";
    }

    @Override
    public void initialize(Bootstrap<AvenueCodeConfiguration> bootstrap)
    {
        // nothing to do yet
    }

    @Override
    public void run(AvenueCodeConfiguration configuration,
                    Environment environment)
    {
        final AvenueCodeResource resource = new AvenueCodeResource();
        environment.jersey().register(resource);
        final HelloWorldResource resource2 = new HelloWorldResource();
        environment.jersey().register(resource2);
       // environment.getApplicationContext().setContextPath("Avenue Code");

        final TemplateHealthCheck healthCheck =
                        new TemplateHealthCheck(configuration.getTemplate());

        // Registers
        environment.healthChecks().register("template", healthCheck);

        environment.jersey().register(resource);
    }

}
