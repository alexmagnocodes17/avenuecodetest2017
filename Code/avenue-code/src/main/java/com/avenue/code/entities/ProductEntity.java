package com.avenue.code.entities;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class ProductEntity extends BaseEntity
{
    @OneToMany(mappedBy = "product", targetEntity = ImageEntity.class,
                    fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<ImageEntity> images;

    @OneToMany(mappedBy = "parentProduct", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Collection<ProductEntity> products;

    @ManyToOne(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
    @JoinColumn(name = "product_parent_id", referencedColumnName = "id")
    private ProductEntity parentProduct;

    @Column(length = 64)
    private String name;

    /**
     * 
     */
    private static final long serialVersionUID = 8700500086371499068L;

    public ProductEntity()
    {
        super();
    }

    /*    *//**
     * @return the images
     */
    /*
     * public List<ImageEntity> getImages() { return images; }
     *//**
     * @param images
     *            the images to set
     */
    /*
     * public void setImages(List<ImageEntity> images) { this.images = images; }
     *//**
     * @return the product
     */
    /*
     * public ProductEntity getProduct() { return product; }
     *//**
     * @param product
     *            the product to set
     */
    /*
     * public void setProduct(ProductEntity product) { this.product = product; }
     *//**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the images
     */
    public Collection<ImageEntity> getImages()
    {
        return images;
    }

    /**
     * @param images
     *            the images to set
     */
    public void setImages(Collection<ImageEntity> images)
    {
        this.images = images;
    }

    /**
     * @return the parentProduct
     */
    public ProductEntity getParentProduct()
    {
        return parentProduct;
    }

    /**
     * @param parentProduct
     *            the parentProduct to set
     */
    public void setParentProduct(ProductEntity parentProduct)
    {
        this.parentProduct = parentProduct;
    }

    /**
     * @return the products
     */
    public Collection<ProductEntity> getProducts()
    {
        return products;
    }

    /**
     * @param products
     *            the products to set
     */
    public void setProducts(Collection<ProductEntity> products)
    {
        this.products = products;
    }
}
