package com.avenue.code.business;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.NoResultException;

import com.avenue.code.builder.ProductBuilder;
import com.avenue.code.dao.ImageDAO;
import com.avenue.code.dao.ProductDAO;
import com.avenue.code.entities.ImageEntity;
import com.avenue.code.entities.ProductEntity;
import com.avenue.code.vo.ImageVO;
import com.avenue.code.vo.ProductVO;

public class ProductBusiness
{
    private ProductDAO productDAO;

    private ImageDAO imageDAO;

    public ProductBusiness()
    {
        productDAO = new ProductDAO();
        imageDAO = new ImageDAO();
    }

    /**
     * @return the productDAO
     */
    public ProductDAO getDAO()
    {
        return productDAO;
    }

    /**
     * @return the imageDAO
     */
    public ImageDAO getImageDAO()
    {
        return imageDAO;
    }


    public ProductVO find(Long id)
    {
        ProductEntity find = getDAO().find(id);

        return new ProductBuilder(find).build();
    }

    public ProductVO parent(Long id)
    {
        ProductEntity find = getDAO().find(id);

        return new ProductBuilder(find).build(null, Boolean.TRUE, null);
    }

    public ProductVO getProductWithImages(Long id)
    {
        ProductEntity find = getDAO().find(id);
        return new ProductBuilder(find).build(null, null, Boolean.TRUE);
    }

    public ProductVO getProductComplete(Long id)
    {
        ProductEntity find = getDAO().find(id);
        return new ProductBuilder(find).build(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
    }

    public void save(ProductVO vo)
    {
        ProductEntity entity = new ProductEntity();
        entity.setName(vo.getName());

        if (vo.getParent() != null)
        {
            ProductEntity parent = getDAO().getEm().find(ProductEntity.class, vo.getParent().getId());
            if (parent != null)
            {
                entity.setParentProduct(parent);
            }
        }

        List<ImageVO> images = vo.getImages();

        List<ImageEntity> collect = null;
        if (images != null && !images.isEmpty())
        {
            collect = images.stream().map(m -> {
                ImageEntity img = new ImageEntity();
                img.setName(m.getName());
                img.setProduct(entity);
                return img;
            }).collect(Collectors.toList());
        }

        if (collect != null && !collect.isEmpty())
            entity.setImages(collect);

        List<ProductVO> leaves2 = vo.getLeaves();
        if (leaves2 != null && !leaves2.isEmpty())
        {
            List<ProductEntity> leaves = leaves2.stream().map(m -> {
                ProductEntity productEntity = new ProductEntity();
                productEntity.setName(m.getName());
                return productEntity;
            }).collect(Collectors.toList());

            entity.setProducts(leaves);
        }
        getDAO().save(entity);
    }

    public void delete(ProductEntity entity)
    {
        getDAO().remove(entity);
    }

    public void delete(Long id)
    {
        getDAO().remove(id);
    }

    public void delete()
    {
        getDAO().remove();
    }

    public void update(ProductVO vo)
    {
        ProductEntity entity = getDAO().find(vo.getId());

        entity.setName(vo.getName());

        if (vo.getParent() != null)
        {
            ProductEntity parent = getDAO().getEm().find(ProductEntity.class, vo.getParent().getId());
            if (parent != null)
            {
                entity.setParentProduct(parent);
            }
        }

        List<ImageVO> images = vo.getImages();

        List<ImageEntity> collect = new ArrayList<ImageEntity>();
        if (images != null && !images.isEmpty())
        {
            images.stream().forEach(m -> {
                ImageEntity x = imageDAO.find(m.getId());
                if (x != null)
                {
                    x.setName(m.getName());
                    collect.add(x);
                }
            });
        }

        if (collect != null && !collect.isEmpty())
            entity.setImages(collect);

        List<ProductVO> leaves2 = vo.getLeaves();
        List<ProductEntity> leaves = new ArrayList<ProductEntity>();
        if (leaves2 != null && !leaves2.isEmpty())
        {
            leaves2.stream().forEach(m -> {
                ProductEntity find = getDAO().find(m.getId());
                if (find != null)
                {
                    find.setName(m.getName());
                    leaves.add(find);
                }
            });

            entity.setProducts(leaves);
        }
        getDAO().update(entity);
    }

    public List<ProductVO> getProducts()
    {
        List<ProductEntity> resultList = Collections.EMPTY_LIST;
        try
        {
            resultList = getDAO().getEm().createQuery("SELECT p FROM ProductEntity p", ProductEntity.class).getResultList();
        }
        catch (NoResultException e)
        {
            // TODO: handle exception
        }

        List<ProductVO> collect = Collections.EMPTY_LIST;
        if (resultList != null && !resultList.isEmpty())
        {
            collect = resultList.stream()
                            .map(m -> {
                                return new ProductBuilder(m).build();
                            }).collect(Collectors.toList());
        }

        return collect;
    }

    public List<ProductVO> getAllParents()
    {
        List<ProductEntity> resultList = Collections.EMPTY_LIST;
        try
        {
            resultList = getDAO().getEm().createQuery("SELECT p FROM ProductEntity p", ProductEntity.class).getResultList();
        }
        catch (NoResultException e)
        {
            // TODO: handle exception
        }

        List<ProductVO> collect = Collections.EMPTY_LIST;
        if (resultList != null && !resultList.isEmpty())
        {
            collect = resultList.stream()
                            .map(m -> {
                                return new ProductBuilder(m).build(null, Boolean.TRUE, null);
                            }).collect(Collectors.toList());
        }

        return collect;
    }

    public List<ProductVO> getAllImages()
    {
        List<ProductEntity> resultList = Collections.EMPTY_LIST;
        try
        {
            resultList = getDAO().getEm().createQuery("SELECT p FROM ProductEntity p", ProductEntity.class).getResultList();
        }
        catch (NoResultException e)
        {
            // TODO: handle exception
        }

        List<ProductVO> collect = Collections.EMPTY_LIST;
        if (resultList != null && !resultList.isEmpty())
        {
            collect = resultList.stream()
                            .map(m -> {
                                return new ProductBuilder(m).build(null, null, Boolean.TRUE);
                            }).collect(Collectors.toList());
        }

        return collect;
    }

    public List<ProductVO> getAllProductsComplete()
    {
        List<ProductEntity> resultList = Collections.EMPTY_LIST;
        try
        {
            resultList = getDAO().getEm().createQuery("SELECT p FROM ProductEntity p", ProductEntity.class).getResultList();
        }
        catch (NoResultException e)
        {
            // TODO: handle exception
        }

        List<ProductVO> collect = Collections.EMPTY_LIST;
        if (resultList != null && !resultList.isEmpty())
        {
            collect = resultList.stream()
                            .map(m -> {
                                return new ProductBuilder(m).build(Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
                            }).collect(Collectors.toList());
        }

        return collect;
    }

    public ProductVO getWithLeaves(Long id)
    {
        ProductEntity singleResult = getDAO().getEm().createQuery("SELECT p FROM ProductEntity p where id = :id", ProductEntity.class).
                        setParameter("id", id).getSingleResult();

        Collection<ProductEntity> productsLeaves = singleResult.getProducts();
        List<ProductVO> collect = Collections.EMPTY_LIST;
        if (productsLeaves != null && !productsLeaves.isEmpty())
        {
            collect = productsLeaves.stream()
                            .map(m -> {
                                return new ProductBuilder(m).build();
                            }).collect(Collectors.toList());
        }

        ProductVO build = new ProductBuilder(singleResult).build();
        build.setLeaves(collect);
        return build;

    }

}
