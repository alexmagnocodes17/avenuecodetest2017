package com.avenue.code.vo;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect  
@JsonIgnoreProperties(ignoreUnknown = true)  
@JsonInclude(JsonInclude.Include.NON_NULL)                  
public class ImageVO
{
    private long id;

    @Length(max = 64)
    private String name;

    public ImageVO()
    {
    }

    public ImageVO(String name)
    {
        this.name = name;
    }

    public ImageVO(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    @JsonProperty
    public long getId()
    {
        return id;
    }

    @JsonProperty
    public String getName()
    {
        return name;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }


}