package com.avenue.code.vo;

import java.util.List;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect  
@JsonIgnoreProperties(ignoreUnknown = true)  
@JsonInclude(JsonInclude.Include.NON_NULL)      
public class ProductVO
{
    private Long id;

    @Length(max = 64)
    private String name;

    private List<ImageVO> images;

    private ProductVO parent;

    private List<ProductVO> leaves;

    public ProductVO()
    {
    }

    public ProductVO(String name)
    {
        this.name = name;
    }

    public ProductVO(long id, String name)
    {
        this.id = id;
        this.name = name;
    }

    @JsonProperty(required = false)
    public long getId()
    {
        return id;
    }

    @JsonProperty
    public String getName()
    {
        return name;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(long id)
    {
        this.id = id;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the images
     */
    @JsonProperty
    public List<ImageVO> getImages()
    {
        return images;
    }

    /**
     * @param images
     *            the images to set
     */
    public void setImages(List<ImageVO> images)
    {
        this.images = images;
    }

    /**
     * @return the parent
     */
    public ProductVO getParent()
    {
        return parent;
    }

    /**
     * @param parent
     *            the parent to set
     */
    public void setParent(ProductVO parent)
    {
        this.parent = parent;
    }

    /**
     * @return the leaves
     */
    public List<ProductVO> getLeaves()
    {
        return leaves;
    }

    /**
     * @param leaves
     *            the leaves to set
     */
    public void setLeaves(List<ProductVO> leaves)
    {
        this.leaves = leaves;
    }

}
