package com.avenue.code.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "image")
public class ImageEntity extends BaseEntity
{
    @Column(name = "name")
    private String name;

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    private ProductEntity product;

    /**
     * 
     */
    private static final long serialVersionUID = -4099555502363152160L;

    public ImageEntity()
    {
        super();
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the product
     */
    public ProductEntity getProduct()
    {
        return product;
    }

    /**
     * @param product
     *            the product to set
     */
    public void setProduct(ProductEntity product)
    {
        this.product = product;
    }


}
