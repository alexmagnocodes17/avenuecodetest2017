package com.avenue.code.dao;

import com.avenue.code.entities.ImageEntity;

/**
 * @author Magno
 *
 */
public class ImageDAO extends GenericDAO<ImageEntity>
{
    public ImageEntity find(Long id)
    {
        return getEm().find(ImageEntity.class, id);
    }

    public void remove(Long id)
    {
        getTransaction().begin();
        ImageEntity find = find(id);
        getEm().remove(find);
        getTransaction()
                        .commit();
    }

    public void remove()
    {
        getTransaction()
                        .begin();
        getEm().createQuery("DELETE FROM ImageEntity");
        getTransaction()
                        .commit();
    }
}
