package com.avenue.code.dao;

import com.avenue.code.entities.ProductEntity;

/**
 * @author Magno
 *
 */
public class ProductDAO extends GenericDAO<ProductEntity>
{
    public ProductEntity find(Long id)
    {
        return getEm().find(ProductEntity.class, id);
    }

    public void remove(Long id)
    {
        getTransaction().begin();
        ProductEntity find = find(id);
        getEm().remove(find);
        getTransaction()
                        .commit();
    }

    public void remove()
    {
        getTransaction()
                        .begin();
        getEm().createQuery("DELETE FROM ProductEntity");
        getTransaction()
                        .commit();
    }
}
