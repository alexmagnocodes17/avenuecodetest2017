package com.avenue.code.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class GenericDAO<T>
{
    public static final String PU = "avenuecode";

    private EntityManagerFactory emf;
    private EntityManager em;

    public GenericDAO()
    {
        emf = Persistence.createEntityManagerFactory(PU);
        em = emf.createEntityManager();

    }

    public void save(T t)
    {
        beginTransaction();
        getEm().persist(t);
        /* getEm().flush(); */
        commitTransaction();
    }

    public void remove(T t)
    {
        beginTransaction();
        getEm().remove(t);
        commitTransaction();
    }

    public void update(T t)
    {
        beginTransaction();
        getEm().merge(t);
        commitTransaction();

    }

    /**
     * @return the emf
     */
    public EntityManagerFactory getEmf()
    {
        return emf;
    }

    /**
     * @return the em
     */
    public EntityManager getEm()
    {
        return em;
    }

    public EntityTransaction getTransaction()
    {
        return getEm().getTransaction();
    }

    public void beginTransaction()
    {
        getTransaction().begin();
    }

    public void commitTransaction()
    {
        if (getTransaction().isActive())
        {
            getTransaction().commit();
        }
    }

    public void rollbackTransaction()
    {
        if (getTransaction().isActive())
        {
            getTransaction().rollback();
        }
    }
}
