package com.avenue.code.resources;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

@Path("/avenue-code/hello")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HelloWorldResource
{
    @GET
    @Path("/helloWorld")
    // http://localhost:8080/hello-world/sayHello?name=magno
    public Response helloWorld()
    {
        try
        {
            return Response.status(Status.OK).entity("Hello World!!").build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error calling helloWorld :(!").build();
        }

    }
}
