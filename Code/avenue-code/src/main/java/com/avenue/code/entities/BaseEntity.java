/**
 * 
 */
package com.avenue.code.entities;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Magno
 *
 */
@MappedSuperclass
public class BaseEntity implements Serializable
{
    /**
     * 
     */
    private static final long serialVersionUID = -8944694667947440553L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "cdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar cDate;

    @Column(name = "mdate")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar mDate;
    
    /**
     * 
     */
    public BaseEntity()
    {
        setcDate(Calendar.getInstance());
    }

    public Long getId()
    {
        return id;
    }

    /**
     * @return the cDate
     */
    public Calendar getcDate()
    {
        return cDate;
    }

    /**
     * @param cDate
     *            the cDate to set
     */
    public void setcDate(Calendar cDate)
    {
        this.cDate = cDate;
    }

    /**
     * @return the mDate
     */
    public Calendar getmDate()
    {
        return mDate;
    }

    /**
     * @param mDate
     *            the mDate to set
     */
    public void setmDate(Calendar mDate)
    {
        this.mDate = mDate;
    }

}
