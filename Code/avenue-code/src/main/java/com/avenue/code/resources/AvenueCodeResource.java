package com.avenue.code.resources;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.avenue.code.business.ProductBusiness;
import com.avenue.code.vo.ProductVO;

@Path("/avenue-code/apis")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AvenueCodeResource
{
    private ProductBusiness productBusiness;

    public AvenueCodeResource()
    {
        productBusiness = new ProductBusiness();
    }

    // traz id, nome
    @GET
    @Path("/product/{id}")
    public Response get(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            ProductVO find = productBusiness.find(opt.get());
            return Response.status(Status.OK).entity(find).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting product!").build();
        }
    }

    // traz id, nome, e o mesmo com os nos filhos.
    @GET
    @Path("/product/{id}/leaves")
    public Response getProductWithLeaves(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            ProductVO withLeaves = productBusiness.getWithLeaves(opt.get());
            return Response.status(Status.OK).entity(withLeaves).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting root and leaves!").build();
        }

    }

    // traz id, nome, e o mesmo com o no pai.
    @GET
    @Path("/product/{id}/parent")
    public Response getProductParent(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            ProductVO find = productBusiness.parent(opt.get());
            return Response.status(Status.OK).entity(find).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting root and parent root!").build();
        }
    }

    // traz id, nome, e o mesmo com o no pai.
    @GET
    @Path("/product/{id}/images")
    public Response getProductImages(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            ProductVO find = productBusiness.getProductWithImages(opt.get());
            return Response.status(Status.OK).entity(find).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting root and parent root!").build();
        }
    }


    // traz id, nome, pai e todas as imagens.
    @GET
    @Path("/product/{id}/parent/images")
    public Response getProductWithImages(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            ProductVO find = productBusiness.getProductComplete(opt.get());
            return Response.status(Status.OK).entity(find).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting product, parent and images!").build();
        }
    }

    // traz lista com id e nome de todos produtos.
    @GET
    @Path("/products")
    public Response get()
    {
        try
        {
            List<ProductVO> products = productBusiness.getProducts();
            return Response.status(Status.OK).entity(products).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting all products!").build();
        }

    }

    // traz lista de todos os n�s e seus respectivos pais.
    @GET
    @Path("/products/parent")
    public Response getAllParents()
    {
        try
        {
            List<ProductVO> products = productBusiness.getAllParents();
            return Response.status(Status.OK).entity(products).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting all products!").build();
        }

    }

    // traz lista de todos os n�s e suas respectivas imagens.
    @GET
    @Path("/products/images")
    public Response getAllImages()
    {
        try
        {
            List<ProductVO> products = productBusiness.getAllImages();
            return Response.status(Status.OK).entity(products).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting all products!").build();
        }
    }

    @GET
    @Path("/products/parent/images")
    public Response getAllProductsComplete()
    {
        try
        {
            List<ProductVO> products = productBusiness.getAllProductsComplete();
            return Response.status(Status.OK).entity(products).build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error getting all products!").build();
        }
    }

    @POST
    @Path("/product")
    public Response insert(ProductVO vo)
    {
        if(vo.getName() == null) {
            return Response.status(400).entity("Please provide the product name!!").build();
        }

        try
        {
            productBusiness.save(vo);
            return Response.status(Status.CREATED).entity("Product saved successful!").build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error saving product!").build();
        }
    }

    @PUT
    @Path("/product")
    public Response update(ProductVO vo)
    {
        try
        {
            productBusiness.update(vo);
            return Response.status(Status.ACCEPTED).entity("Product updated successful!").build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error updating product!").build();
        }
    }

    @DELETE
    @Path("/product/{id}")
    public Response delete(@PathParam("id") Optional<Long> opt)
    {
        try
        {
            productBusiness.delete(opt.get());
            return Response.status(Status.NO_CONTENT).entity("Product deleted successful!").build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error deleting product!").build();
        }
    }

    @DELETE
    @Path("/products")
    public Response delete()
    {
        try
        {
            productBusiness.delete();
            return Response.status(Status.CREATED).entity("Truncated table products successful!").build();
        }
        catch (Exception e)
        {
            return Response.status(Status.BAD_REQUEST).entity("Error truncating table products!").build();
        }
    }

}
