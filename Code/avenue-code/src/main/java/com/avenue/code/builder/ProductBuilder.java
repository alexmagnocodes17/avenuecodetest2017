package com.avenue.code.builder;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import com.avenue.code.entities.ProductEntity;
import com.avenue.code.vo.ImageVO;
import com.avenue.code.vo.ProductVO;

public class ProductBuilder
{
    private ProductEntity entity;

    public ProductBuilder(ProductEntity entity)
    {
        this.entity = entity;
    }

    public ProductVO build()
    {
        ProductVO productVO = new ProductVO();
        productVO.setName(entity.getName());
        productVO.setId(entity.getId());
        return productVO;
    }

    public ProductVO build(Boolean withLeaves, Boolean withParent, Boolean withImages)
    {
        ProductVO productVO = build();
        if (withLeaves != null && withLeaves)
        {
            List<ProductVO> collect = null;
            Collection<ProductEntity> products = entity.getProducts();

            if (products != null && !products.isEmpty())
            {
                collect = products
                                .stream()
                                .map(m -> {
                                    ProductVO vo = new ProductVO();
                                    vo.setId(m.getId());
                                    vo.setName(m.getName());
                                    return vo;
                                }).collect(Collectors.toList());
            }
            productVO.setLeaves(collect);
        }

        if (withParent != null && withParent)
        {
            ProductEntity parentProduct = entity.getParentProduct();
            if (parentProduct != null)
            {
                ProductVO parent = new ProductVO();
                parent.setId(parentProduct.getId());
                parent.setName(parentProduct.getName());
                productVO.setParent(parent);
            }
        }

        if (withImages != null && withImages)
        {
            List<ImageVO> collect = null;
            if (entity.getImages() != null)
            {
                collect = entity.getImages()
                                .stream()
                                .map(m -> {
                                    ImageVO img = new ImageVO();
                                    img.setId(m.getId());
                                    img.setName(m.getName());
                                    return img;
                                }).collect(Collectors.toList());
            }
            productVO.setImages(collect);
        }
        return productVO;
    }
}
