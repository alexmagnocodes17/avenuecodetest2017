package com.avenue.code.main;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.avenue.code.builder.ProductBuilder;
import com.avenue.code.dao.ProductDAO;
import com.avenue.code.entities.ImageEntity;
import com.avenue.code.entities.ProductEntity;
import com.avenue.code.vo.ImageVO;
import com.avenue.code.vo.ProductVO;

public class Main
{

    public static void main(String[] args)
    {

        ProductDAO dao = new ProductDAO();
        ProductEntity product = new ProductEntity();
        product.setName("DoMeio");
        
        ImageEntity e1 = new ImageEntity();
        e1.setName("IMAGE_01.png");
        ImageEntity e2 = new ImageEntity();
        e2.setName("IMAGE_02.png");
        ImageEntity e3 = new ImageEntity();
        e3.setName("IMAGE_03.png");
        
        product.setImages(Arrays.asList(e1, e2, e3));

        ProductEntity parentProduct = new ProductEntity();
        parentProduct.setName("Pai");
        
        ProductEntity productChild = new ProductEntity();
        productChild.setName("Filho");

        product.setParentProduct(parentProduct);

        product.setProducts(Arrays.asList(productChild));

        dao.save(product);
        
        ProductEntity find = dao.find(1L);

        ProductEntity find2 = dao.find(2L);
        
        ProductEntity find3 = dao.find(3L);
        
        System.out.println("Alow");

        /*List<ProductEntity> resultList = dao.getEm().createQuery("SELECT p FROM ProductEntity p", ProductEntity.class).getResultList();
        
        resultList.forEach( reg -> {
            System.out.println(reg.getId() + "" + reg.getName());
        });*/

    }

}
