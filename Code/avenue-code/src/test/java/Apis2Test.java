import static org.junit.Assert.assertEquals;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.ClientProperties;
import org.junit.ClassRule;
import org.junit.Test;

import com.avenue.code.dropwizard.AvenueCodeApplication;
import com.avenue.code.dropwizard.AvenueCodeConfiguration;

/**
 * @author Magno
 *
 */
public class Apis2Test
{
    @ClassRule
    public static final DropwizardAppRule<AvenueCodeConfiguration> RULE =
                    new DropwizardAppRule<AvenueCodeConfiguration>(AvenueCodeApplication.class, ResourceHelpers.resourceFilePath("example.yml"));

    @Test
    public void testHelloWorld()
    {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");
        client.property(ClientProperties.CONNECT_TIMEOUT, 30000);
        client.property(ClientProperties.READ_TIMEOUT, 30000);

        WebTarget resource = target("/avenue-code/hello/helloWorld", client);        
        Response response = resource.request().get();
        assertEquals(response.getStatus(), 200);
    }
    
   /* @Test
    public void postProduct()
    {
        Client client = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");
        client.property(ClientProperties.CONNECT_TIMEOUT, 30000);
        client.property(ClientProperties.READ_TIMEOUT, 30000);
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        WebTarget resource = target("/avenue-code/apis/product", client);        
        ProductVO vo = new ProductVO("Product Magno");
        Response response = resource.request(MediaType.APPLICATION_JSON).post(Entity.entity(vo, MediaType.APPLICATION_JSON));
        assertEquals(response.getStatus(), 201);
    }
*/
    public WebTarget target(String path, Client client)
    {
        String url = String.format("http://localhost:%d%s", RULE.getLocalPort(), path);
        return client.target(url);
    }
    
}
